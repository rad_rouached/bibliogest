# biblioGest
gestion de bibliotheques / library management 
------------------------------
JAVA ENTREPRISE EDITION / JAVA EE


#Gestion de bibliothéque multilangues: 
Notre application consiste a développer une application multilangues destiné pour la gestion de bibliothéque (mediatheque) qui contient 4 grandes parties  :+1:  

* Gestion des emprunts 
* Gestion des adhérents 
* Gestion des documents 
* Gestion des supports
* Statiques

 Nos principaux acteurs sont : 
* Administrateur
* Adhérent

Les technologies utilisées : 
Hibernate,JSTL ,i18n, JSP, Design patterns (patron de conception): MVC (modèle-vue-contrôleur) ,fabrique abstraite

***********************************************************************

#Library management multilingual:
Our application consists in developing an application designed to manage the library that contains four parts: 1:

* Loan Management
* Manage members
* Document Management
* Support Management
* Statics

Our main actors are:
* Admin
* Member

Technologies used:
Hibernate, JSTL, i18n, JSP , Design patterns: MVC (model-view-controller) ,abstract factory

*** Radhouene Rouached
*#Enjoy n' Share !*
